package me.nuno1212s.customrecipes.recipes;

import lombok.AllArgsConstructor;
import lombok.Data;
import me.nuno1212s.customrecipes.nms.CCraftingManager;
import me.nuno1212s.customrecipes.nms.NMSRegister;
import me.nuno1212s.customrecipes.nms.Recipe;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Serializable Recipe
 */
@AllArgsConstructor
@Data
public class SerializableRecipe {

    ItemStack[] result;

    HashMap<Character, ItemStack> items;

    Type t;

    String permission;

    String recipeID;

    String[] shape;

    Recipe r;


    public SerializableRecipe(String recipeID, Type t, ItemStack[] result, HashMap<Character, ItemStack> items, String permission, String... shape) {
        this.recipeID = recipeID;
        this.t = t;
        this.result = result;
        this.items = items;
        this.permission = permission;
        this.shape = shape;
        this.r = NMSRegister.getRecipe(this, shape);
    }

    public SerializableRecipe(ConfigurationSection section) {
        this.recipeID = section.getName();
        ConfigurationSection results = section.getConfigurationSection("Results");
        this.result = new ItemStack[results.getKeys(false).size()];
        int ii = 0;
        for (String key : results.getKeys(false)) {
            result[ii] = results.getItemStack(key);
            ii++;
        }
        this.items = new HashMap<>();
        this.permission = section.getString("Permission");
        this.t = Type.valueOf(section.getString("Type"));
        ConfigurationSection items = section.getConfigurationSection("Items");
        for (String s : items.getKeys(false)) {
            this.items.put(s.charAt(0), items.getItemStack(s));
        }
        String[] shape = section.getString("Shape").split(",");
        this.shape = shape;
        this.r = NMSRegister.getRecipe(this, shape[0], shape[1], shape[2]);
    }

    public void unRegister() {
        CCraftingManager.unregister(this.r);
    }

    public void save(ConfigurationSection s) {
        ConfigurationSection cs = s.createSection("Results");
        int item = 0;
        for (ItemStack i : this.result) {
            cs.set(String.valueOf(item), i);
            item++;
        }
        s.set("Permission", this.permission);
        s.set("Type", this.t.name());
        if (this.t == Type.SHAPED) {
            String[] shape = {"123", "456", "789"};
            for (int i = 0; i < shape.length; i++) {
                for (int ii = 0; ii < shape[i].length(); ii++) {
                    if (this.items.get(shape[i].charAt(ii)) == null) {
                        shape[i] = shape[i].replace(shape[i].charAt(ii), ' ');
                    }
                }
            }
            String shap = "";
            for (String str : shape) {
                shap += str + ",";
            }
            s.set("Shape", shap);
        }
        ConfigurationSection items = s.getConfigurationSection("Items");
        if (items == null) {
            items = s.createSection("Items");
        }
        for (Map.Entry<Character, ItemStack> entry : this.items.entrySet()) {
            items.set(entry.getKey().toString(), entry.getValue());
        }
    }

    public void update(HashMap<Character, ItemStack> items, ItemStack[] result, String... shape) {
        CCraftingManager.unregister(this.r);
        this.shape = shape;
        this.items = items;
        this.result = result;
        this.r = NMSRegister.getRecipe(this, shape);
        RecipeManager.getIns().rebuildFile(this);
    }


}
