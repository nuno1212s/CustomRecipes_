package me.nuno1212s.customrecipes.recipes;

import lombok.Getter;
import me.nuno1212s.customrecipes.main.Main;
import me.nuno1212s.customrecipes.nms.CCraftingManager;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Manages recipes
 */
public class RecipeManager {

    private static RecipeManager ins;

    public static RecipeManager getIns() {
        return ins;
    }

    @Getter
    List<SerializableRecipe> recipes;

    private File f;

    private Main m;

    public RecipeManager(Main m) {
        this.m = m;
        ins = this;
        recipes = new ArrayList<>();
        if (!m.getDataFolder().exists()) {
            m.getDataFolder().mkdirs();
        }
        this.f = loadFile();
        FileConfiguration fc = YamlConfiguration.loadConfiguration(f);
        ConfigurationSection s = fc.getConfigurationSection("Recipes");
        if (s == null) {
            return;
        }
        for (String keys : s.getKeys(false)) {
            SerializableRecipe r = new SerializableRecipe(s.getConfigurationSection(keys));
            this.recipes.add(r);
        }
        CCraftingManager.sort();
    }

    private File loadFile() {
        File f = new File(m.getDataFolder(), "recipes.yml");
        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return f;
    }

    public SerializableRecipe getRecipeFor(ItemStack result, ItemStack[] matrix) {
        List<SerializableRecipe> re = new ArrayList<>();
        for (SerializableRecipe r : this.recipes) {
            for (ItemStack i : r.getResult()) {
                if (equals(i, result) && i.getAmount() == result.getAmount()) {
                    re.add(r);
                }
            }
        }
        if (re.size() == 1) {
            return re.get(0);
        } else if (re.isEmpty()) {
            return null;
        } else {
            SerializableRecipe bestFit = re.get(0);
            int currentMatches = 0;
            for (SerializableRecipe r : re) {
                int matches = 0;
                if (r.getT() == Type.SHAPED) {
                    for (char c : r.getItems().keySet()) {
                        int instance = Integer.parseInt(Character.toString(c)) - 1;
                        if (matrix[instance] == null) {
                            matches = 0;
                            break;
                        }
                        if (!equals(matrix[instance], r.getItems().get(c)) || matrix[instance].getAmount() < r.getItems().get(c).getAmount()) {
                            matches = 0;
                            break;
                        }
                        matches++;
                    }
                } else {
                    Collection<ItemStack> values = r.getItems().values();
                    for (ItemStack i : matrix) {
                        boolean flag = false;
                        Iterator<ItemStack> iterator = values.iterator();
                        while (iterator.hasNext()) {
                            ItemStack ii = iterator.next();
                            if (equals(i, ii)) {
                                flag = true;
                                values.remove(ii);
                                break;
                            }
                        }

                        if (!flag) {
                            matches = 0;
                            break;
                        } else {
                            matches++;
                        }
                    }
                }
                if (matches > currentMatches) {
                    bestFit = r;
                    currentMatches = matches;
                }
            }
            return bestFit;
        }
    }

    public String createRecipe(String name, HashMap<Character, ItemStack> items, ItemStack[] result, Type t, String permission, String... shape) {
        SerializableRecipe r = new SerializableRecipe(name, t, result, items, permission, shape);
        this.recipes.add(r);
        this.save(r);
        return name;
    }

    void save(SerializableRecipe r) {
        Bukkit.getScheduler().runTaskAsynchronously(m, new Runnable() {
            @Override
            public void run() {
                f = loadFile();

                FileConfiguration fc = YamlConfiguration.loadConfiguration(f);
                ConfigurationSection s = fc.getConfigurationSection("Recipes");
                if (s == null) {
                    s = fc.createSection("Recipes");
                }
                ConfigurationSection recipe = s.createSection(String.valueOf(r.getRecipeID()));
                r.save(recipe);

                try {
                    fc.save(f);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void remove(SerializableRecipe r) {
        this.recipes.remove(r);
        removeFile(r);
    }

    public void rebuildFile(SerializableRecipe r) {
        Bukkit.getScheduler().runTaskAsynchronously(m, new Runnable() {
            @Override
            public void run() {
                f = loadFile();
                FileConfiguration fc = YamlConfiguration.loadConfiguration(f);
                ConfigurationSection s = fc.getConfigurationSection("Recipes");
                if (s == null) {
                    s = fc.createSection("Recipes");
                }
                s.set(String.valueOf(r.getRecipeID()), null);
                r.save(s.createSection(r.getRecipeID()));
                try {
                    fc.save(f);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    void removeFile(SerializableRecipe r) {
        Bukkit.getScheduler().runTaskAsynchronously(m, new Runnable() {
            @Override
            public void run() {
                f = loadFile();
                FileConfiguration fc = YamlConfiguration.loadConfiguration(f);
                ConfigurationSection s = fc.getConfigurationSection("Recipes");
                if (s == null) {
                    s = fc.createSection("Recipes");
                }
                s.set(String.valueOf(r.getRecipeID()), null);
                try {
                    fc.save(f);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });
    }


    public SerializableRecipe getByID(String id) {
        for (SerializableRecipe s : this.recipes) {
            if (s.getRecipeID().equalsIgnoreCase(id)) {
                return s;
            }
        }
        return null;
    }


    public boolean equals(ItemStack item1, ItemStack item2) {
        return !(item1 == null || item2 == null) && item1.getType() == item2.getType() && item1.getData().getData() == item2.getData().getData() && item1.hasItemMeta() == item2.hasItemMeta() && (Bukkit.getItemFactory().equals(item1.getItemMeta(), item2.getItemMeta()));
    }

    /**
     * Get the recipes by page
     *
     * @param page Starts at 1
     * @param recipesPerPage The amount of recipes per page
     * @return The recipes
     */
    public List<SerializableRecipe> byPage(int page, int recipesPerPage) {
        List<SerializableRecipe> recipes = new ArrayList<>(this.recipes), pageRecipes = new ArrayList<>();
        int pageAmount = getPages(recipesPerPage), ceil = page * recipesPerPage;
        if (page > pageAmount || page <= 0) {
            return null;
        }
        if (ceil >= recipes.size()) {
            ceil = recipes.size();
        }
        for (int recipe = (page - 1) * recipesPerPage; recipe < ceil; recipe++) {
            pageRecipes.add(recipes.get(recipe));
        }
        return pageRecipes;
    }

    public int getPages(int recipesPerPage) {
        return (int) Math.floor((double) (this.recipes.size() / recipesPerPage) + 1);
    }

}
