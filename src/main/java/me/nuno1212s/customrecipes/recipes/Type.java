package me.nuno1212s.customrecipes.recipes;

/**
 * Type of recipes
 */
public enum Type {
    SHAPELESS,
    SHAPED
}
