package me.nuno1212s.customrecipes.nms;

import me.nuno1212s.customrecipes.main.Main;
import me.nuno1212s.customrecipes.recipes.SerializableRecipe;
import me.nuno1212s.customrecipes.recipes.Type;
import me.nuno1212s.customrecipes.reflection.ReflectionManager;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;


/**
 * Registers the correct classes depending on the version
 */
@SuppressWarnings("unchecked")
public class NMSRegister {

    public static Recipe getRecipe(SerializableRecipe r, String... shape) {
        if (r.getT() == Type.SHAPED) {
            Class c = ReflectionManager.getClass("me.nuno1212s.customrecipes.nms." + Main.getVersion() + "SShapedRecipe");

            if (c == null) {
                return null;
            }

            Constructor constructor = ReflectionManager.getConstructor(c, r.getResult().getClass(), r.getPermission().getClass());

            Object b = ReflectionManager.newInstance(constructor, r.getResult(), r.getPermission());

            Method shapeMethod = ReflectionManager.getMethod(c, "shape", shape.getClass());

            ReflectionManager.invoke(shapeMethod, b, ((Object)shape));

            Method setIngredient = ReflectionManager.getMethod(c, "setIngredient", char.class, ItemStack.class);

            for (char ch : r.getItems().keySet()) {
                ReflectionManager.invoke(setIngredient, b, ch, r.getItems().get(ch));
            }

            Method addToCrafting = ReflectionManager.getMethod(c, "addToCraftingManager");
            ReflectionManager.invoke(addToCrafting, b);

            return (Recipe) ReflectionManager.invoke(ReflectionManager.getMethod(c, "getS"), b);

        } else {
            Class c = ReflectionManager.getClass("me.nuno1212s.customrecipes.nms." + Main.getVersion() + "SShapelessRecipe");

            if (c == null) {
                return null;
            }

            Constructor constructor = ReflectionManager.getConstructor(c, r.getResult().getClass(), r.getPermission().getClass());

            Object b = ReflectionManager.newInstance(constructor, r.getResult(), r.getPermission());

            Method addIngredient = ReflectionManager.getMethod(c, "addIngredient", ItemStack.class);

            r.getItems().values().forEach(i ->
                    ReflectionManager.invoke(addIngredient, b, i)
            );

            Method addToCrafting = ReflectionManager.getMethod(c, "addToCraftingManager");
            ReflectionManager.invoke(addToCrafting, b);

            return (Recipe) ReflectionManager.invoke(ReflectionManager.getMethod(c, "getR"), b);
        }
    }


}
