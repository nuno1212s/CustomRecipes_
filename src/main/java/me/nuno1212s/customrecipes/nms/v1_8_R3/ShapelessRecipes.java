package me.nuno1212s.customrecipes.nms.v1_8_R3;

import com.google.common.collect.Lists;
import me.nuno1212s.customrecipes.nms.Recipe;
import net.minecraft.server.v1_8_R3.InventoryCrafting;
import net.minecraft.server.v1_8_R3.ItemStack;
import net.minecraft.server.v1_8_R3.World;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 */
public class ShapelessRecipes extends net.minecraft.server.v1_8_R3.ShapelessRecipes implements Recipe {

    List<ItemStack> ingredients;
    String permission;
    ItemStack[] results;

    public ShapelessRecipes(ItemStack[] result, List<ItemStack> list, String permission) {
        super(result[0], list);
        this.permission = permission;
        this.ingredients = list;
        this.results = result;
    }


    @Override
    public boolean a(InventoryCrafting inventorycrafting, World world) {
        ArrayList arraylist = Lists.newArrayList(this.ingredients);

        if (!inventorycrafting.getViewers().isEmpty()) {
            if (!inventorycrafting.getViewers().get(0).hasPermission(this.permission)) {
                return false;
            }
        }

        for (int i = 0; i < inventorycrafting.h(); ++i) {
            for (int j = 0; j < inventorycrafting.i(); ++j) {
                ItemStack itemstack = inventorycrafting.c(j, i);
                if (itemstack != null) {
                    boolean flag = false;
                    Iterator iterator = arraylist.iterator();

                    while (iterator.hasNext()) {
                        ItemStack itemstack1 = (ItemStack) iterator.next();
                        org.bukkit.inventory.ItemStack item1 = CraftItemStack.asBukkitCopy(itemstack), item2 = CraftItemStack.asBukkitCopy(itemstack1);
                        if (itemstack.getItem() == itemstack1.getItem() && (itemstack1.getData() == 32767 || itemstack.getData() == itemstack1.getData()) && item1.getAmount() >= item2.getAmount() &&
                                Bukkit.getItemFactory().equals(item1.getItemMeta(), item2.getItemMeta())) {
                            flag = true;
                            arraylist.remove(itemstack1);
                            break;
                        }
                    }

                    if (!flag) {
                        return false;
                    }
                }
            }
        }

        return arraylist.isEmpty();
    }

    @Override
    public ItemStack craftItem(InventoryCrafting inventorycrafting) {
        return this.b().cloneItemStack();
    }

    @Override
    public ItemStack b() {
        return this.results[new Random().nextInt(this.results.length)];
    }
}
