package me.nuno1212s.customrecipes.nms.v1_8_R3;

import me.nuno1212s.customrecipes.nms.Recipe;
import net.minecraft.server.v1_8_R3.InventoryCrafting;
import net.minecraft.server.v1_8_R3.ItemStack;
import net.minecraft.server.v1_8_R3.World;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;

import java.util.Random;


/**
 * Shaped recipe
 */
public class ShapedRecipes extends net.minecraft.server.v1_8_R3.ShapedRecipes implements Recipe {

    private int width, height;
    private ItemStack[] items;
    private ItemStack[] results;
    private String permission;

    public ShapedRecipes(int width, int height, ItemStack[] items, ItemStack[] result, String permission) {
        super(width, height, items, result[0]);
        this.results = result;
        this.width = width;
        this.height = height;
        this.items = items;
        this.permission = permission;
    }

    @Override
    public boolean a(InventoryCrafting inventorycrafting, World world) {
        for(int i = 0; i <= 3 - width; ++i) {
            for(int j = 0; j <= 3 - height; ++j) {
                if(this.a(inventorycrafting, i, j, true)) {
                    return true;
                }

                if(this.a(inventorycrafting, i, j, false)) {
                    return true;
                }
            }
        }

        return false;
    }

    private boolean a(InventoryCrafting inventoryCrafting, int i, int j, boolean flag) {
        if (!inventoryCrafting.getViewers().isEmpty()) {
            if (!inventoryCrafting.getViewers().get(0).hasPermission(this.permission)) {
                return false;
            }
        }
        for(int k = 0; k < 3; ++k) {
            for(int l = 0; l < 3; ++l) {
                int i1 = k - i;
                int j1 = l - j;
                ItemStack itemstack = null;
                if(i1 >= 0 && j1 >= 0 && i1 < this.width && j1 < this.height) {
                    if(flag) {
                        itemstack = this.items[this.width - i1 - 1 + j1 * this.width];
                    } else {
                        itemstack = this.items[i1 + j1 * this.width];
                    }
                }

                ItemStack itemstack1 = inventoryCrafting.c(k, l);
                org.bukkit.inventory.ItemStack item1 = CraftItemStack.asBukkitCopy(itemstack), item2 = CraftItemStack.asBukkitCopy(itemstack1);
                if(itemstack1 != null || itemstack != null) {
                    if(itemstack1 == null && itemstack != null || itemstack1 != null && itemstack == null) {
                        return false;
                    }

                    if(itemstack.getItem() != itemstack1.getItem()) {
                        return false;
                    }

                    if(itemstack.getData() != 32767 && itemstack.getData() != itemstack1.getData()) {
                        return false;
                    }

                    if (item1.getAmount() > item2.getAmount()) {
                        return false;
                    }

                    if (!Bukkit.getItemFactory().equals(item1.getItemMeta(), item2.getItemMeta())) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    @Override
    public ItemStack b() {
        return this.results[new Random().nextInt(this.results.length)];
    }
}
