package me.nuno1212s.customrecipes.nms.v1_8_R3;

import lombok.Getter;
import me.nuno1212s.customrecipes.nms.CCraftingManager;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftRecipe;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapelessRecipe;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Shapeless recipe
 */
public class SShapelessRecipe extends ShapelessRecipe implements CraftRecipe{

    String permission;

    @Getter
    ShapelessRecipes r;
    ItemStack[] results;

    public SShapelessRecipe(ItemStack[] result, String permission) {
        super(result[0]);
        this.results = result;
        this.permission = permission;
    }

    public SShapelessRecipe addIngredient(ItemStack i) {
        Field f;
        Class cl = this.getClass().getSuperclass();
        try {
            f = cl.getDeclaredField("ingredients");
            f.setAccessible(true);
            List<ItemStack> ingredientList = getIngredientList();
            ingredientList.add(i);
            f.set(this, ingredientList);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return this;
    }

    public void addToCraftingManager() {
        List<ItemStack> ingred = this.getIngredientList();
        Object[] data = new Object[ingred.size()];
        int i = 0;
        for (ItemStack mdata : ingred) {
            data[i] = CraftItemStack.asNMSCopy(mdata);
            i++;
        }
        net.minecraft.server.v1_8_R3.ItemStack[] items = new net.minecraft.server.v1_8_R3.ItemStack[this.results.length];
        int ii = 0;
        for (ItemStack item : this.results) {
            items[ii] = CraftItemStack.asNMSCopy(item);
            ii++;
        }
        this.r = CCraftingManager.registerShapelessRecipe(items, permission, data);
    }

}
