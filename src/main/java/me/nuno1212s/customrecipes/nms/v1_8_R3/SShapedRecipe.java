package me.nuno1212s.customrecipes.nms.v1_8_R3;

import lombok.Getter;
import me.nuno1212s.customrecipes.nms.CCraftingManager;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftRecipe;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;

import java.lang.reflect.Field;
import java.util.Map;

/**
 * Shaped recipes
 */
public class SShapedRecipe extends ShapedRecipe implements CraftRecipe {

    @Getter
    ShapedRecipes s;
    String permission;

    ItemStack[] results;

    public SShapedRecipe(ItemStack[] result, String permission) {
        super(result[0] == null ? (result[1] == null ? result[2] : result[1]) : result[0]);
        this.permission = permission;
        this.results = result;
    }

    public ItemStack[] getResults() {
        return this.results;
    }

    public static SShapedRecipe fromBukkitRecipe(ShapedRecipe recipe) {
        if (recipe instanceof SShapedRecipe) {
            return (SShapedRecipe) recipe;
        }
        SShapedRecipe ret = new SShapedRecipe(new ItemStack[]{recipe.getResult()}, "");
        String[] shape = recipe.getShape();
        ret.shape(shape);
        Map<Character, ItemStack> ingredientMap = recipe.getIngredientMap();
        for (char c : ingredientMap.keySet()) {
            ItemStack stack = ingredientMap.get(c);
            if (stack != null) {
                ret.setIngredient(c, stack);
            }
        }
        return ret;
    }

    public SShapedRecipe setIngredient(char c, ItemStack i) {
        Class cl = this.getClass().getSuperclass();
        try {
            Field f = cl.getDeclaredField("ingredients");
            f.setAccessible(true);
            Map<Character, ItemStack> ingredientMap = getIngredientMap();
            ingredientMap.put(c, i);
            f.set(this, ingredientMap);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return this;

    }

    public void addToCraftingManager() {
        Object[] data;
        String[] shape = this.getShape();
        Map<Character, ItemStack> ingred = this.getIngredientMap();
        int datalen = shape.length;
        datalen += ingred.size() * 2;
        int i = 0;
        data = new Object[datalen];
        for (; i < shape.length; i++) {
            data[i] = shape[i];
        }
        for (char c : ingred.keySet()) {
            ItemStack mdata = ingred.get(c);
            if (mdata == null) continue;
            data[i] = c;
            i++;
            data[i] = CraftItemStack.asNMSCopy(mdata);
            i++;
        }
        net.minecraft.server.v1_8_R3.ItemStack[] items = new net.minecraft.server.v1_8_R3.ItemStack[this.results.length];
        int ii = 0;
        for (ItemStack item : this.results) {
            items[ii] = CraftItemStack.asNMSCopy(item);
            ii++;
        }
        this.s = CCraftingManager.registerShapedRecipe(items, this.permission, data);
    }

}
