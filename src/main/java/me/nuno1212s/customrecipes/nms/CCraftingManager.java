package me.nuno1212s.customrecipes.nms;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import me.nuno1212s.customrecipes.main.Main;
import me.nuno1212s.customrecipes.nms.v1_8_R3.ShapedRecipes;
import me.nuno1212s.customrecipes.nms.v1_8_R3.ShapelessRecipes;
import me.nuno1212s.customrecipes.reflection.ReflectionManager;
import net.minecraft.server.v1_8_R3.*;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 */
@SuppressWarnings("unchecked")
public class CCraftingManager {


    public static void unregister(Recipe r) {
        Class c = ReflectionManager.getClass("net.minecraft.server." + Main.getVersion() + "CraftingManager");
        if (c == null) {
            return;
        }
        Object o = ReflectionManager.invoke(ReflectionManager.getMethod(c, "getInstance"), null);
        List<IRecipe> recipes = (List<IRecipe>) ReflectionManager.invoke(ReflectionManager.getMethod(c, "getRecipes"), o);
        assert recipes != null;
        recipes.remove(r);
        ReflectionManager.setField(ReflectionManager.getField(c, "recipes"), o, recipes);
    }

    public static ShapedRecipes registerShapedRecipe(ItemStack[] itemstack, String permission, Object... aobject) {
        String s = "";
        int i = 0;
        int j = 0;
        int k = 0;
        if (aobject[i] instanceof String[]) {
            String[] var11 = (String[]) aobject[i++];

            for (int aitemstack = 0; aitemstack < var11.length; ++aitemstack) {
                String shapedrecipes = var11[aitemstack];
                ++k;
                j = shapedrecipes.length();
                s = s + shapedrecipes;
            }
        } else {
            while (aobject[i] instanceof String) {
                String hashmap = (String) aobject[i++];
                ++k;
                j = hashmap.length();
                s = s + hashmap;
            }
        }

        HashMap var12;
        for (var12 = Maps.newHashMap(); i < aobject.length; i += 2) {
            Character var13 = (Character) aobject[i];
            ItemStack var15 = null;
            if (aobject[i + 1] instanceof Item) {
                var15 = new ItemStack((Item) aobject[i + 1]);
            } else if (aobject[i + 1] instanceof Block) {
                var15 = new ItemStack((Block) aobject[i + 1], 1, 32767);
            } else if (aobject[i + 1] instanceof ItemStack) {
                var15 = (ItemStack) aobject[i + 1];
            }

            var12.put(var13, var15);
        }

        ItemStack[] var14 = new ItemStack[j * k];

        for (int var16 = 0; var16 < j * k; ++var16) {
            char c0 = s.charAt(var16);
            if (var12.containsKey(Character.valueOf(c0))) {
                var14[var16] = ((ItemStack) var12.get(Character.valueOf(c0))).cloneItemStack();
            } else {
                var14[var16] = null;
            }
        }

        ShapedRecipes var17 = new ShapedRecipes(j, k, var14, itemstack, permission);
        //Reflection Start
        Class c = ReflectionManager.getClass("net.minecraft.server." + Main.getVersion() + "CraftingManager");
        Object o = ReflectionManager.invoke(ReflectionManager.getMethod(c, "getInstance"), null);
        Field recipe = ReflectionManager.getField(c, "recipes");
        List<IRecipe> recipes = (List<IRecipe>) ReflectionManager.getFromField(recipe, o);
        assert recipes != null;
        recipes.add(var17);
        ReflectionManager.setField(recipe, o, recipes);
        //Reflection end
        return var17;
    }

    public static ShapelessRecipes registerShapelessRecipe(ItemStack[] itemstack, String permission, Object... aobject) {
        ArrayList arraylist = Lists.newArrayList();
        Object[] aobject1 = aobject;
        int i = aobject.length;

        for (int j = 0; j < i; ++j) {
            Object object = aobject1[j];
            if (object instanceof ItemStack) {
                arraylist.add(((ItemStack) object).cloneItemStack());
            } else if (object instanceof Item) {
                arraylist.add(new ItemStack((Item) object));
            } else {
                if (!(object instanceof Block)) {
                    throw new IllegalArgumentException("Invalid shapeless recipe: unknown type " + object.getClass().getName() + "!");
                }

                arraylist.add(new ItemStack((Block) object));
            }
        }

        ShapelessRecipes shapelessRecipess = new ShapelessRecipes(itemstack, arraylist, permission);
        //Reflection Start
        Class c = ReflectionManager.getClass("net.minecraft.server." + Main.getVersion() + "CraftingManager");
        Object o = ReflectionManager.invoke(ReflectionManager.getMethod(c, "getInstance"), null);
        Field recipe = ReflectionManager.getField(c, "recipes");
        List<IRecipe> recipes = (List<IRecipe>) ReflectionManager.getFromField(recipe, o);
        assert recipes != null;
        recipes.add(shapelessRecipess);
        ReflectionManager.setField(recipe, o, recipes);
        //Reflection end
        return shapelessRecipess;
    }


    public static void sort() {
        CraftingManager.getInstance().sort();
    }
}
