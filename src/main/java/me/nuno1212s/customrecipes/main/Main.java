package me.nuno1212s.customrecipes.main;

import me.nuno1212s.customrecipes.createrecipe.CommandManager;
import me.nuno1212s.customrecipes.eventmanager.PrepareRecipeEvent;
import me.nuno1212s.customrecipes.recipeinventory.CreateRecipeInventory;
import me.nuno1212s.customrecipes.recipeinventory.EditRecipeInventory;
import me.nuno1212s.customrecipes.recipes.RecipeManager;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Main class
 */
public class Main extends JavaPlugin{

    @Override
    public void onEnable() {
        new RecipeManager(this);
        new CreateRecipeInventory(this);
        new EditRecipeInventory(this);
        getCommand("recipe").setExecutor(new CommandManager());
        Bukkit.getPluginManager().registerEvents(new PrepareRecipeEvent(this), this);
    }

    public static String getVersion() {
        String name = Bukkit.getServer().getClass().getPackage().getName();
        String mcVersion = name.substring(name.lastIndexOf('.') + 1).replace("org.bukkit.craftbukkit.", "");
        return mcVersion + ".";
    }

}
