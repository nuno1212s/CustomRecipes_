package me.nuno1212s.customrecipes.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Manages reflection
 *
 * (handles all of the exceptions)
 */
@SuppressWarnings("unchecked")
public class ReflectionManager {

    private static Map<String, Class<?>> loadedclasses = new HashMap<>();

    private static Map<Class<?>, Map<String, Map<Class<?>[], Method>>> methods = new HashMap<>();

    private static Map<Class<?>, Map<String, Field>> fields = new HashMap<>();

    private static Map<Class<?>, Map<Class<?>[], Constructor>> constructors = new HashMap<>();

    public static Class<?> getClass(String name) {
        if (loadedclasses.containsKey(name)) {
            return loadedclasses.get(name);
        }
        try {
            Class<?> clas = Class.forName(name);
            loadedclasses.put(name, clas);
            return clas;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            loadedclasses.put(name, null);
        }
        return null;
    }

    public static Object invoke(Method m, Object o, Object... arguments) {
        try {
            return m.invoke(o, arguments);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Method getMethod(Class c, String name, Class<?>... arguments) {
        if (methods.containsKey(c)) {
            if (methods.get(c).containsKey(name)) {
                if (methods.get(c).get(name).containsKey(arguments)) {
                    return methods.get(c).get(name).get(arguments);
                }
            }
        }
        try {
            Method method = c.getMethod(name, arguments);
            if (methods.containsKey(c)) {
                Map<String, Map<Class<?>[], Method>> map = methods.get(c);
                if (map.containsKey(name)) {
                    Map<Class<?>[], Method> map2 = map.get(name);
                    map2.put(arguments, method);
                    map.put(name, map2);
                    methods.put(c, map);
                } else {
                    Map<Class<?>[], Method> m = new HashMap<>();
                    m.put(arguments, method);
                    map.put(name, m);
                    methods.put(c, map);
                }
            } else {
                Map<String, Map<Class<?>[], Method>> map = new HashMap<>();
                Map<Class<?>[], Method> m = new HashMap<>();
                m.put(arguments, method);
                map.put(name, m);
                methods.put(c, map);
            }
            return method;
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Field getField(Class c, String name) {
        if (fields.containsKey(c)) {
            if (fields.get(c).containsKey(name)) {
                return fields.get(c).get(name);
            }
        }
        try {
            Field f = c.getField(name);
            if (fields.containsKey(c)) {
                Map<String, Field> map = fields.get(c);
                map.put(name, f);
                fields.put(c, map);
            } else {
                Map<String, Field> map = new HashMap<>();
                map.put(name, f);
                fields.put(c, map);
            }
            return f;
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void setField(Field f, Object o, Object set) {
        try {
            f.set(o, set);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public static Object getFromField(Field f, Object o) {
        try {
            return f.get(o);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Constructor getConstructor(Class c, Class<?>... parameters) {
        if (constructors.containsKey(c)) {
            if (constructors.get(c).containsKey(parameters)) {
                return constructors.get(c).get(parameters);
            }
        }
        try {
            Constructor constructor = c.getDeclaredConstructor(parameters);
            if (constructors.containsKey(c)) {
                Map<Class<?>[], Constructor> map = constructors.get(c);
                map.put(parameters, constructor);
                constructors.put(c, map);
            } else {
                Map<Class<?>[], Constructor> map = new HashMap<>();
                map.put(parameters, constructor);
                constructors.put(c, map);
            }
            return constructor;
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Object newInstance(Constructor c, Object... parameters) {
        try {
            return c.newInstance(parameters);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

}
