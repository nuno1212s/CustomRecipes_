package me.nuno1212s.customrecipes.createrecipe;

import org.bukkit.command.CommandSender;

/**
 * Command Interface
 */
interface Command {

    void onCommand(CommandSender sender, org.bukkit.command.Command cmd, String label, String[] args);
}
