package me.nuno1212s.customrecipes.createrecipe;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.AbstractQueue;

/**
 * Manages all commands
 */
public class CommandManager implements CommandExecutor {

    private CreateRecipe c;
    private DeleteRecipe d;
    private EditRecipe e;
    private ListRecipes l;
    private MoreInformation m;
    private ChangePermission p;

    public CommandManager() {
        c = new CreateRecipe();
        d = new DeleteRecipe();
        e = new EditRecipe();
        l = new ListRecipes();
        m = new MoreInformation();
        p = new ChangePermission();
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender.isOp() || commandSender.hasPermission("customrecipes.commands") || commandSender.hasPermission("*") || commandSender.hasPermission("customrecipes.*")) {
            if (strings.length == 0) {
                commandSender.sendMessage("");
                commandSender.sendMessage(ChatColor.AQUA + "/recipe createrecipe <Name> <Type> -p <Permission> " + ChatColor.WHITE + "-" + ChatColor.YELLOW + " Create a recipe");
                commandSender.sendMessage(ChatColor.AQUA + "/recipe deleterecipe <Name> " + ChatColor.WHITE + "-" + ChatColor.YELLOW + " Delete a recipe");
                commandSender.sendMessage(ChatColor.AQUA + "/recipe editrecipe <Recipe Name> " + ChatColor.WHITE + "-" + ChatColor.YELLOW + " Edit a recipe");
                commandSender.sendMessage(ChatColor.AQUA + "/recipe listrecipes " + ChatColor.WHITE + "-" + ChatColor.YELLOW + " List all the recipes");
                commandSender.sendMessage(ChatColor.AQUA + "/recipe moreinfo <Recipe Name> " + ChatColor.WHITE + "-" + ChatColor.YELLOW + " More info about a recipe");
                commandSender.sendMessage(ChatColor.AQUA + "/recipe changepermission <Name> -p <Permission> " + ChatColor.WHITE + "-" + ChatColor.YELLOW + " Change the permission of a recipe");
                commandSender.sendMessage("");
                return true;
            }
            switch (strings[0].toUpperCase()) {
                case "CREATERECIPE": {
                    c.onCommand(commandSender, command, s, strings);
                    break;
                }
                case "DELETERECIPE": {
                    d.onCommand(commandSender, command, s, strings);
                    break;
                }
                case "EDITRECIPE": {
                    e.onCommand(commandSender, command, s, strings);
                    break;
                }
                case "LISTRECIPES": {
                    l.onCommand(commandSender, command, s, strings);
                    break;
                }
                case "MOREINFO": {
                    m.onCommand(commandSender, command, s, strings);
                    break;
                }
                case "CHANGEPERM": {
                    p.onCommand(commandSender, command, s, strings);
                    break;
                }
                default: {
                    commandSender.sendMessage("");
                    commandSender.sendMessage(ChatColor.AQUA + "/recipe createrecipe <Name> <Type> -p <Permission> " + ChatColor.WHITE + "-" + ChatColor.YELLOW + " Create a recipe");
                    commandSender.sendMessage(ChatColor.AQUA + "/recipe deleterecipe <Name> " + ChatColor.WHITE + "-" + ChatColor.YELLOW + " Delete a recipe");
                    commandSender.sendMessage(ChatColor.AQUA + "/recipe editrecipe <Recipe Name> " + ChatColor.WHITE + "-" + ChatColor.YELLOW + " Edit a recipe");
                    commandSender.sendMessage(ChatColor.AQUA + "/recipe listrecipes " + ChatColor.WHITE + "-" + ChatColor.YELLOW + " List all the recipes");
                    commandSender.sendMessage(ChatColor.AQUA + "/recipe moreinfo <Recipe Name> " + ChatColor.WHITE + "-" + ChatColor.YELLOW + " More info about a recipe");
                    commandSender.sendMessage(ChatColor.AQUA + "/recipe changepermission <Name> -p <Permission> " + ChatColor.WHITE + "-" + ChatColor.YELLOW + " Change the permission of a recipe");
                    commandSender.sendMessage("");
                    break;
                }
            }
        }
        return true;
    }
}
