package me.nuno1212s.customrecipes.createrecipe;

import me.nuno1212s.customrecipes.recipeinventory.EditRecipeInventory;
import me.nuno1212s.customrecipes.recipes.RecipeManager;
import me.nuno1212s.customrecipes.recipes.SerializableRecipe;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Edit recipe command
 */
class EditRecipe implements Command {

    @Override
    public void onCommand(CommandSender sender, org.bukkit.command.Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "This command can only be executed by a player");
            return;
        }
        if (sender.hasPermission("customrecipes.edit") || sender.isOp() || sender.hasPermission("*") || sender.hasPermission("customrecipes.*")) {
            if (args.length < 2) {
                sender.sendMessage(ChatColor.RED + "/recipe editrecipe <RecipeName>");
                return;
            }
            String name = args[1];
            SerializableRecipe r = RecipeManager.getIns().getByID(name);
            if (r == null) {
                sender.sendMessage(ChatColor.RED + "There is no recipe with that name!");
                return;
            }
            ((Player) sender).openInventory(EditRecipeInventory.getIns().getInventory(r));
        }
    }
}
