package me.nuno1212s.customrecipes.createrecipe;

import me.nuno1212s.customrecipes.recipes.RecipeManager;
import me.nuno1212s.customrecipes.recipes.SerializableRecipe;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

/**
 * Deletes a recipe
 */
class DeleteRecipe implements Command{

    @Override
    public void onCommand(CommandSender sender, org.bukkit.command.Command cmd, String label, String[] args) {
        if (sender.hasPermission("customrecipes.delete") || sender.hasPermission("*") || sender.hasPermission("customrecipes.*") || sender.isOp()) {
            if (args.length < 2) {
                sender.sendMessage(ChatColor.RED + "/recipe deleterecipe <recipe name>");
                return;
            }
            SerializableRecipe s = RecipeManager.getIns().getByID(args[1]);
            if (s == null) {
                sender.sendMessage(ChatColor.RED + "Name not recognised!");
                return;
            }
            s.unRegister();
            RecipeManager.getIns().remove(s);
            sender.sendMessage(ChatColor.RED + "Recipe " + ChatColor.YELLOW + args[1] + ChatColor.RED + " removed!");
        }
    }
}
