package me.nuno1212s.customrecipes.createrecipe;

import me.nuno1212s.customrecipes.Fanciful.FancyMessage;
import me.nuno1212s.customrecipes.recipes.RecipeManager;
import me.nuno1212s.customrecipes.recipes.SerializableRecipe;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * List recipes
 */
class ListRecipes implements Command{

    public void onCommand(CommandSender sender, org.bukkit.command.Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "THis command cannot be performed by the console!");
            return;
        }
        if (sender.hasPermission("customrecipes.list") || sender.isOp() || sender.hasPermission("customrecipes.*")) {
            if (RecipeManager.getIns().getRecipes().isEmpty()) {
                sender.sendMessage(ChatColor.RED + "There are currently no recipes available. Add them with " + ChatColor.GRAY + "/recipe createrecipe");
                return;
            }
            int page = (args.length == 2) ? Integer.parseInt(args[1]) : 1;
            if (page == 0) {
                sender.sendMessage(ChatColor.RED + "There is no page 0");
                return;
            } else if (page > RecipeManager.getIns().getPages(10)) {
                sender.sendMessage(ChatColor.RED + "The page number is too high!");
                return;
            }
            sender.sendMessage(ChatColor.AQUA + "Recipes:");
            List<SerializableRecipe> recipes = RecipeManager.getIns().byPage(page, 10);
            for (SerializableRecipe r : recipes) {
                new FancyMessage().text(r.getRecipeID()).color(ChatColor.AQUA).command("/recipe moreinfo " + r.getRecipeID()).tooltip("Click for more info").then(" (Click for more info)").command("/recipe moreinfo " + r.getRecipeID()).tooltip("Click for more info").color(ChatColor.GRAY).send(sender);
            }
            if (page > 1 && page < RecipeManager.getIns().getPages(10)) {
                new FancyMessage().text("<-- ").color(ChatColor.GOLD).tooltip("Previous page").command("/recipe listrecipes " + String.valueOf(page - 1))
                        .then().text("     " + String.valueOf(page) + "      ").color(ChatColor.GOLD).style(ChatColor.BOLD).then().text("-->").color(ChatColor.GOLD).tooltip("Next page").command("/recipe listrecipes " + String.valueOf(page + 1)).send(sender);
            } else if (page == 1 && page < RecipeManager.getIns().getPages(10)) {
                new FancyMessage().text("--- ").color(ChatColor.GOLD).then().text("     " + String.valueOf(page) + "      ").color(ChatColor.GOLD).style(ChatColor.BOLD).then().text("-->").color(ChatColor.GOLD).tooltip("Next page").command("/recipe listrecipes " + String.valueOf(page + 1)).send(sender);
            } else if (page == 1 && page >= RecipeManager.getIns().getPages(10)) {
                new FancyMessage().text("--- ").color(ChatColor.GOLD).then().text("     " + String.valueOf(page) + "      ").color(ChatColor.GOLD).style(ChatColor.BOLD).then().text("---").color(ChatColor.GOLD).send(sender);
            } else if (page >= RecipeManager.getIns().getPages(10) && page > 1) {
                new FancyMessage().text("<-- ").color(ChatColor.GOLD).tooltip("Previous page").command("/recipe listrecipes " + String.valueOf(page - 1))
                        .then().text("     " + String.valueOf(page) + "      ").color(ChatColor.GOLD).style(ChatColor.BOLD).then().text("---").color(ChatColor.GOLD).send(sender);
            }
        }
    }
}
