package me.nuno1212s.customrecipes.createrecipe;

import com.sun.org.apache.regexp.internal.RE;
import me.nuno1212s.customrecipes.Fanciful.FancyMessage;
import me.nuno1212s.customrecipes.recipes.RecipeManager;
import me.nuno1212s.customrecipes.recipes.SerializableRecipe;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;

/**
 * Shows information for a recipe
 */
class MoreInformation implements Command{

    @Override
    public void onCommand(CommandSender sender, org.bukkit.command.Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "This command can only be performed by a player!");
            return;
        }
        if (sender.hasPermission("customrecipes.moreinfo") || sender.hasPermission("*") || sender.hasPermission("customrecipes.*") || sender.isOp()) {
            if (args.length < 2) {
                sender.sendMessage("/recipe moreinfo <Name>");
                return;
            }
            String name = args[1];
            SerializableRecipe r = RecipeManager.getIns().getByID(name);
            if (r == null) {
                sender.sendMessage(ChatColor.RED + "That recipe does not exist!");
                return;
            }
            sender.sendMessage(ChatColor.AQUA + "Recipe Info:");
            sender.sendMessage(ChatColor.GRAY + "Name: " + ChatColor.YELLOW + r.getRecipeID());
            new FancyMessage()
                    .text("Permission: ")
                    .color(ChatColor.GRAY)
                    .tooltip("Click to change")
                    .suggest("/recipe changeperm " + r.getRecipeID() + " -p ")
                    .then()
                    .text((r.getPermission().equalsIgnoreCase("") ? "None" : r.getPermission()))
                    .color(ChatColor.YELLOW).suggest("/recipe changeperm " + r.getRecipeID() + " -p ")
                    .tooltip("Click to change")
                    .send(sender);
            sender.sendMessage(ChatColor.GRAY + "Type: " + ChatColor.YELLOW + r.getT().name());
            sender.sendMessage(ChatColor.GRAY + "Results: ");
            for (ItemStack i : r.getResult()) {
                sendItem((Player)sender, i);
            }
            sender.sendMessage(ChatColor.GRAY + "Shape:");
            for (String s : r.getShape()) {
                sender.sendMessage(s.replace(" ", "-"));
            }
            sender.sendMessage(ChatColor.GRAY + "Items:");
            for (char c : r.getItems().keySet()) {
                sender.sendMessage(Character.toString(c) + ":");
                sendItem((Player) sender, r.getItems().get(c));
            }
            new FancyMessage().text("Click to edit").color(ChatColor.GOLD).command("/recipe EDITRECIPE " + r.getRecipeID()).tooltip("Click to edit the recipe").send(sender);
            new FancyMessage().text("Click to delete").color(ChatColor.RED).suggest("/recipe deleterecipe " + r.getRecipeID()).tooltip(Arrays.asList("Click to delete the recipe", "Requires further confirmation")).send(sender);
        }
    }

    private void sendItem(Player p, ItemStack i) {
        new FancyMessage().text("     x" + i.getAmount() + " " + i.getType().name().replace("_", " ")).color(ChatColor.GOLD).itemTooltip(i).send(p);
    }

}
