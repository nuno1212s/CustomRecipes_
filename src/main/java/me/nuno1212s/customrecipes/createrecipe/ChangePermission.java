package me.nuno1212s.customrecipes.createrecipe;

import me.nuno1212s.customrecipes.recipes.RecipeManager;
import me.nuno1212s.customrecipes.recipes.SerializableRecipe;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

/**
 * Change the permission of a recipe
 */
public class ChangePermission implements Command {

    @Override
    public void onCommand(CommandSender sender, org.bukkit.command.Command cmd, String label, String[] args) {
        if (sender.hasPermission("customrecipes.changepermission") || sender.hasPermission("customrecipes.*") || sender.isOp() || sender.hasPermission("*")) {
            if (args.length == 1) {
                sender.sendMessage(ChatColor.RED + "/recipe changeperm <Name> -p <Permission>");
                return;
            }
            else if (args.length > 1) {
                SerializableRecipe r = RecipeManager.getIns().getByID(args[1]);
                if (r == null) {
                    sender.sendMessage(ChatColor.RED + "That recipe does not exist. Try " + ChatColor.GRAY + "/recipe listrecipes" + ChatColor.RED + " to see the available recipes!");
                    return;
                }
                String permission;
                if (args.length == 2) {
                    permission = "";
                } else if (args.length > 2) {
                    if (args[2].equalsIgnoreCase("-p")) {
                        if (args.length >= 4) {
                            permission = args[3];
                        } else {
                            permission = "";
                        }
                    } else {
                        permission = "";
                    }
                } else {
                    permission = "";
                }
                r.setPermission(permission);
                RecipeManager.getIns().rebuildFile(r);
                sender.sendMessage(ChatColor.RED + "You changed the permission of the recipe " + ChatColor.GRAY + r.getRecipeID() + ChatColor.RED + " to " + ChatColor.GOLD + (permission.equalsIgnoreCase("") ? "None" : permission));
            }
        }
    }
}
