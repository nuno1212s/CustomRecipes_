package me.nuno1212s.customrecipes.createrecipe;


import me.nuno1212s.customrecipes.recipeinventory.CreateRecipeInventory;
import me.nuno1212s.customrecipes.recipes.RecipeManager;
import me.nuno1212s.customrecipes.recipes.Type;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

/**
 * Create Recipe command
 */
class CreateRecipe implements me.nuno1212s.customrecipes.createrecipe.Command {

    @Override
    public void onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (!(commandSender instanceof Player)) {
            return;
        }
        if (strings.length < 3) {
            commandSender.sendMessage(ChatColor.RED + "/recipe createrecipe <Name> <Type> -p <Permission>");
            return;
        }
        if (commandSender.hasPermission("customrecipes.create") || commandSender.isOp() || commandSender.hasPermission("*") || commandSender.hasPermission("customrecipes.*")) {
            Player p = (Player) commandSender;
            String name = strings[1];
            if (RecipeManager.getIns().getByID(name) != null) {
                p.sendMessage(ChatColor.RED + "A recipe with that name already exists");
                return;
            }
            /*String[] shape = {"123", "456", "789"};
            HashMap<Character, ItemStack> items = new HashMap<>();
            ItemStack[] results = new ItemStack[3];
            int size = 0;
            if (p.getInventory().getItem(21) != null && p.getInventory().getItem(21).getType() != Material.AIR) {
                results[size] = p.getInventory().getItem(21);
                size++;
            }
            if (p.getInventory().getItem(12) != null && p.getInventory().getItem(12).getType() != Material.AIR) {
                results[size] = p.getInventory().getItem(12);
                size++;
            }
            if (p.getInventory().getItem(30) != null && p.getInventory().getItem(30).getType() != Material.AIR) {
                results[size] = p.getInventory().getItem(30);
                size++;
            }
            ItemStack[] result = new ItemStack[size];
            switch (size) {
                case 1: {
                    result[0] = results[0] == null ? (results[1] == null ? results[2] : results[1]) : results[0];
                    break;
                }
                case 2: {
                    result[0] = results[0] == null ? (results[1] == null ? results[2] : results[1]) : results[0];
                    result[1] = results[1] == null ? results[2] : results[1];
                    break;
                }
                case 3: {
                    result[0] = p.getInventory().getItem(12);
                    result[1] = p.getInventory().getItem(21);
                    result[2] = p.getInventory().getItem(30);
                    break;
                }
                default:
                    break;
            }
            if (result.length == 0) {
                p.sendMessage(ChatColor.RED + "You have no result item!");
                return;
            }
            int initialSlot = 9;
            for (int row = 1; row <= 3; row++) {
                for (int slot = 1; slot <= 3; slot++) {
                    int finalSlot = (initialSlot * row + slot) - 1;
                    ItemStack i = p.getInventory().getItem(finalSlot);
                    if (i == null || i.getType() == Material.AIR) {
                        shape[row - 1] = shape[row - 1].replace(shape[row - 1].charAt(slot - 1), ' ');
                        continue;
                    }
                    ItemStack ii = i.clone();
                    items.put(shape[row - 1].charAt(slot - 1), ii);
                }
            }*/
            Type t = Type.valueOf(strings[2].toUpperCase());
            String permission;
            if (strings.length == 3) {
                permission = "";
            } else if (strings.length > 3) {
                if (strings[3].equalsIgnoreCase("-p")) {
                    if (strings.length >= 5) {
                        permission = strings[4];
                    } else {
                        permission = "";
                    }
                } else {
                    permission = "";
                }
            } else {
                permission = "";
            }
            p.openInventory(CreateRecipeInventory.getIns().getInventory(name, permission, t));
            //String id = RecipeManager.getIns().createRecipe(name, items, result, t, permission, shape);
            //p.sendMessage(ChatColor.GREEN + "Recipe created with the name: " + ChatColor.YELLOW + id);
        }
    }
}
