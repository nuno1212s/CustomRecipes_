package me.nuno1212s.customrecipes.eventmanager;

import me.nuno1212s.customrecipes.main.Main;
import me.nuno1212s.customrecipes.recipes.RecipeManager;
import me.nuno1212s.customrecipes.recipes.SerializableRecipe;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Prepare Recipe Event
 */
public class PrepareRecipeEvent implements Listener {

    Main m;

    public PrepareRecipeEvent(Main m) {
        this.m = m;
    }

    @EventHandler
    public void oncraft(CraftItemEvent e) {
        SerializableRecipe r = RecipeManager.getIns().getRecipeFor(e.getRecipe().getResult(), e.getInventory().getMatrix());
        if (r == null) {
            return;
        }
        ArrayList<ItemStack> values = new ArrayList<>(r.getItems().values());
        ItemStack[] newMatrix = e.getInventory().getMatrix().clone();
        for (int i = 0; i < newMatrix.length; i++) {
            if (newMatrix[i] == null) {
                continue;
            }
            Iterator<ItemStack> iterator = values.iterator();
            while (iterator.hasNext()) {
                ItemStack items = iterator.next();
                ItemStack item = newMatrix[i].clone();
                if (RecipeManager.getIns().equals(items, item) && item.getAmount() >= items.getAmount()) {
                    if (item.getAmount() == items.getAmount()) {
                        newMatrix[i] = new ItemStack(Material.BARRIER);
                    } else {
                        item.setAmount(item.getAmount() - items.getAmount() + 1);
                        newMatrix[i] = item;
                    }
                    values.remove(items);
                    break;
                }
            }
        }
        for (int i = 0; i < newMatrix.length; i++) {
            if (newMatrix[i] == null) {
                continue;
            }
            if (newMatrix[i].getType() == Material.BARRIER) {
                newMatrix[i] = null;
            }
        }
        e.getInventory().setMatrix(newMatrix);
        ((Player) e.getWhoClicked()).updateInventory();
    }

}
