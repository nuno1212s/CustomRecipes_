package me.nuno1212s.customrecipes.recipeinventory;

import me.nuno1212s.customrecipes.main.Main;
import me.nuno1212s.customrecipes.recipes.RecipeManager;
import me.nuno1212s.customrecipes.recipes.SerializableRecipe;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

/**
 * Handles the editing of recipes
 */
public class EditRecipeInventory implements Listener{

    static EditRecipeInventory ins;

    public static EditRecipeInventory getIns() {
        return ins;
    }

    public EditRecipeInventory(Main m) {
        ins = this;
        Bukkit.getServer().getPluginManager().registerEvents(this, m);
    }

    public Inventory getInventory(SerializableRecipe r) {
        String[] shape = r.getShape();
        Inventory i = Bukkit.createInventory(null, 27, ChatColor.RED + "Recipe Edit Inventory");
        for (int row = 0; row < 3; row++) {
            for (int slot = 0; slot < 3; slot++) {
                int finalslot = 9 * row + slot;
                char c = shape[row].charAt(slot);
                ItemStack item = r.getItems().get(c);
                i.setItem(finalslot, item);
            }
        }
        int current = 4;
        for (ItemStack resultitem : r.getResult()) {
            i.setItem(current, resultitem);
            current += 9;
        }
        {
            ItemStack confirm = new ItemStack(Material.WOOL, 1, (short) 5);
            ItemMeta m = confirm.getItemMeta();
            m.setDisplayName(ChatColor.GREEN + "Confirm");
            m.setLore(Arrays.asList(ChatColor.GREEN + "Edit the recipe",
                    ChatColor.GREEN + "Name: " + ChatColor.GRAY + r.getRecipeID(),
                    ChatColor.GREEN + "Permission: " + ChatColor.GRAY + (r.getPermission() == null || r.getPermission().equalsIgnoreCase("") ? "None" : r.getPermission()),
                    ChatColor.GREEN + "Type: " + ChatColor.GRAY + r.getT().name()));
            confirm.setItemMeta(m);
            i.setItem(15, confirm);
        }

        {
            ItemStack cancel = new ItemStack(Material.WOOL, 1, (short) 14);
            ItemMeta m = cancel.getItemMeta();
            m.setDisplayName(ChatColor.RED + "Cancel");
            m.setLore(Collections.singletonList(ChatColor.RED + "Cancel the edition"));
            cancel.setItemMeta(m);
            i.setItem(17, cancel);
        }

        {
            ItemStack creates = new ItemStack(Material.SKULL_ITEM, 1, (short) SkullType.PLAYER.ordinal());
            SkullMeta m = (SkullMeta) creates.getItemMeta();
            m.setDisplayName(ChatColor.RED + "->");
            m.setLore(Collections.singletonList(ChatColor.RED + "->"));
            m.setOwner("MHF_ArrowRight");
            creates.setItemMeta(m);
            i.setItem(12, creates);
        }
        return i;
    }

    @EventHandler
    public void clickInventory(InventoryClickEvent e) {
        if (e.getCurrentItem() == null || e.getCurrentItem().getType() == Material.AIR || !e.getCurrentItem().hasItemMeta()) {
            return;
        }
        if (e.getInventory().getName().equalsIgnoreCase(ChatColor.RED + "Recipe Edit Inventory")) {
            if (e.getCurrentItem().getType() == Material.WOOL) {
                if (e.getCurrentItem().getDurability() == 5 && e.getRawSlot() == 15) {
                    e.setCancelled(true);
                    String name = e.getCurrentItem().getItemMeta().getLore().get(1).replace(ChatColor.GREEN + "Name: " + ChatColor.GRAY, "");
                    SerializableRecipe r = RecipeManager.getIns().getByID(name);
                    String[] shape = {"123", "456", "789"};
                    HashMap<Character, ItemStack> items = new HashMap<>();
                    ItemStack[] results = new ItemStack[3];
                    int size = 0;
                    Inventory inv = e.getClickedInventory();
                    if (inv.getItem(13) != null && inv.getItem(13).getType() != Material.AIR) {
                        results[size] = inv.getItem(13);
                        size++;
                    }
                    if (inv.getItem(4) != null && inv.getItem(4).getType() != Material.AIR) {
                        results[size] = inv.getItem(4);
                        size++;
                    }
                    if (inv.getItem(22) != null && inv.getItem(22).getType() != Material.AIR) {
                        results[size] = inv.getItem(22);
                        size++;
                    }
                    ItemStack[] result = new ItemStack[size];
                    switch (size) {
                        case 1: {
                            result[0] = results[0] == null ? (results[1] == null ? results[2] : results[1]) : results[0];
                            break;
                        }
                        case 2: {
                            result[0] = results[0] == null ? (results[1] == null ? results[2] : results[1]) : results[0];
                            result[1] = results[1] == null ? results[2] : results[1];
                            break;
                        }
                        case 3: {
                            result[0] = inv.getItem(4);
                            result[1] = inv.getItem(13);
                            result[2] = inv.getItem(22);
                            break;
                        }
                        default:
                            break;
                    }
                    if (result.length == 0) {
                        e.getWhoClicked().sendMessage(ChatColor.RED + "You have no result item!");
                        return;
                    }
                    for (int row = 0; row < 3; row++) {
                        for (int slot = 0; slot < 3; slot++) {
                            int finalSlot = 9 * row + slot;
                            ItemStack i = inv.getItem(finalSlot);
                            if (i == null || i.getType() == Material.AIR) {
                                shape[row] = shape[row].replace(shape[row ].charAt(slot), ' ');
                                continue;
                            }
                            ItemStack ii = i.clone();
                            items.put(shape[row].charAt(slot), ii);
                        }
                    }
                    r.update(items, result, shape);
                    e.getWhoClicked().closeInventory();
                    e.getWhoClicked().sendMessage(ChatColor.GREEN + "Edited the recipe: " + ChatColor.GRAY + name);
                } else if (e.getCurrentItem().getDurability() == 14 && e.getRawSlot() == 17) {
                    e.setCancelled(true);
                    e.getWhoClicked().closeInventory();
                }
            }
        }
    }
}
