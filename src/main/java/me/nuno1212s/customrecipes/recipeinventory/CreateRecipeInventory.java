package me.nuno1212s.customrecipes.recipeinventory;

import me.nuno1212s.customrecipes.main.Main;
import me.nuno1212s.customrecipes.recipes.RecipeManager;
import me.nuno1212s.customrecipes.recipes.Type;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

/**
 * Create recipe inventory
 * <p>
 * Handles the create of recipes through an inventory
 */
public class CreateRecipeInventory implements Listener {

    static CreateRecipeInventory ins;

    public static CreateRecipeInventory getIns() {
        return ins;
    }

    public CreateRecipeInventory(Main m) {
        ins = this;
        Bukkit.getPluginManager().registerEvents(this, m);
    }

    public Inventory getInventory(String name, String permission, Type t) {
        Inventory inv = Bukkit.getServer().createInventory(null, 27, ChatColor.DARK_GRAY + "Recipe Create Inventory");
        {
            ItemStack confirm = new ItemStack(Material.WOOL, 1, (short) 5);
            ItemMeta m = confirm.getItemMeta();
            m.setDisplayName(ChatColor.GREEN + "Confirm");
            m.setLore(Arrays.asList(ChatColor.GREEN + "Create the recipe",
                    ChatColor.GREEN + "Name: " + ChatColor.GRAY + name,
                    ChatColor.GREEN + "Permission: " + ChatColor.GRAY + (permission == null || permission.equalsIgnoreCase("") ? "None" : permission),
                    ChatColor.GREEN + "Type: " + ChatColor.GRAY + t.name()));
            confirm.setItemMeta(m);
            inv.setItem(15, confirm);
        }

        {
            ItemStack cancel = new ItemStack(Material.WOOL, 1, (short) 14);
            ItemMeta m = cancel.getItemMeta();
            m.setDisplayName(ChatColor.RED + "Cancel");
            m.setLore(Collections.singletonList(ChatColor.RED + "Cancel the creation"));
            cancel.setItemMeta(m);
            inv.setItem(17, cancel);
        }

        {
            ItemStack creates = new ItemStack(Material.SKULL_ITEM, 1, (short) SkullType.PLAYER.ordinal());
            SkullMeta m = (SkullMeta) creates.getItemMeta();
            m.setDisplayName(ChatColor.RED + "->");
            m.setLore(Collections.singletonList(ChatColor.RED + "->"));
            m.setOwner("MHF_ArrowRight");
            creates.setItemMeta(m);
            inv.setItem(12, creates);
        }
        return inv;
    }

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        if (e.getInventory().getName().equalsIgnoreCase(ChatColor.DARK_GRAY + "Recipe Create Inventory")) {
            if (e.getCurrentItem().getType() == Material.WOOL) {
                if (e.getCurrentItem().getDurability() == 14 && e.getRawSlot() == 17) {
                    e.setCancelled(true);
                    e.getWhoClicked().closeInventory();
                    for (int row = 1; row <= 3; row++) {
                        for (int slot = 1; slot <= 3; slot ++) {
                            int finalslot = row * slot - 1;
                            if (e.getClickedInventory().getItem(finalslot) != null && e.getClickedInventory().getItem(finalslot).getType() != Material.AIR)
                            e.getWhoClicked().getInventory().addItem(e.getClickedInventory().getItem(finalslot));
                        }
                    }
                    for (int result = 4; result <= 22; result += 9) {
                        if (e.getClickedInventory().getItem(result) != null && e.getClickedInventory().getItem(result).getType() != Material.AIR)
                            e.getWhoClicked().getInventory().addItem(e.getClickedInventory().getItem(result));
                    }

                } else if (e.getCurrentItem().getDurability() == 5 && e.getRawSlot() == 15) {
                    e.setCancelled(true);
                    e.getWhoClicked().closeInventory();
                    String name = e.getCurrentItem().getItemMeta().getLore().get(1).replace(ChatColor.GREEN + "Name: " + ChatColor.GRAY, "");
                    String permission = e.getCurrentItem().getItemMeta().getLore().get(2).replace(ChatColor.GREEN + "Permission: " + ChatColor.GRAY, "");
                    if (permission.equalsIgnoreCase("None")) {
                        permission = "";
                    }
                    Type t = Type.valueOf(e.getCurrentItem().getItemMeta().getLore().get(3).replace(ChatColor.GREEN + "Type: " + ChatColor.GRAY, ""));
                    String[] shape = {"123", "456", "789"};
                    HashMap<Character, ItemStack> items = new HashMap<>();
                    ItemStack[] results = new ItemStack[3];
                    int size = 0;
                    Inventory inv = e.getClickedInventory();
                    if (inv.getItem(13) != null && inv.getItem(13).getType() != Material.AIR) {
                        results[size] = inv.getItem(13);
                        size++;
                    }
                    if (inv.getItem(4) != null && inv.getItem(4).getType() != Material.AIR) {
                        results[size] = inv.getItem(4);
                        size++;
                    }
                    if (inv.getItem(22) != null && inv.getItem(22).getType() != Material.AIR) {
                        results[size] = inv.getItem(22);
                        size++;
                    }
                    ItemStack[] result = new ItemStack[size];
                    switch (size) {
                        case 1: {
                            result[0] = results[0] == null ? (results[1] == null ? results[2] : results[1]) : results[0];
                            break;
                        }
                        case 2: {
                            result[0] = results[0] == null ? (results[1] == null ? results[2] : results[1]) : results[0];
                            result[1] = results[1] == null ? results[2] : results[1];
                            break;
                        }
                        case 3: {
                            result[0] = inv.getItem(4);
                            result[1] = inv.getItem(13);
                            result[2] = inv.getItem(22);
                            break;
                        }
                        default:
                            break;
                    }
                    if (result.length == 0) {
                        e.getWhoClicked().sendMessage(ChatColor.RED + "You have no result item!");
                        return;
                    }
                    for (int row = 0; row < 3; row++) {
                        for (int slot = 0; slot < 3; slot++) {
                            int finalSlot = 9 * row + slot;
                            ItemStack i = inv.getItem(finalSlot);
                            if (i == null || i.getType() == Material.AIR) {
                                shape[row] = shape[row].replace(shape[row ].charAt(slot), ' ');
                                continue;
                            }
                            ItemStack ii = i.clone();
                            items.put(shape[row].charAt(slot), ii);
                        }
                    }
                    String id = RecipeManager.getIns().createRecipe(name, items, result, t, permission, shape);
                    e.getWhoClicked().sendMessage(ChatColor.GREEN + "Created recipe with the name: " + ChatColor.GRAY + id);
                }
            }
        }
    }

}
